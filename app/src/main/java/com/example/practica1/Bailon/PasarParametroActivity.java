package com.example.practica1.Bailon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class PasarParametroActivity extends AppCompatActivity {

    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        cajaDatos =(EditText)findViewById(R.id.editTextParametro);
        botonEnviar = (Button)findViewById(R.id.buttonEnviarParametro);
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametroActivity.this, RecibirParametroActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}
